﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

//Modified from: http://msdn.microsoft.com/en-us/library/dd460720(v=vs.110).aspx

namespace ImageFlip
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleKeyInfo c;

            PrintMenu();

            while ((c = Console.ReadKey(true)) != null)
            {
                switch (Char.ToLower(c.KeyChar))
                {
                    case '1':
                        SequentialFlipper();
                        break;
                    case '2':
                        ParallelFlipper();
                        break;
                    case 'q':
                        return;
                }

                PrintMenu();
            }
        }

        static void SequentialFlipper()
        {
            Stopwatch s = new Stopwatch();
            s.Start();

            // A simple source for demonstration purposes. Modify this path as necessary. 
            string[] files = System.IO.Directory.GetFiles(@"Sample Pictures", "*.jpg");
            string newDir = @"Sample Pictures\Modified";
            System.IO.Directory.CreateDirectory(newDir);

            //  Method signature: Parallel.ForEach(IEnumerable<TSource> source, Action<TSource> body)
            foreach(string currentFile in files)
            {
                // The more computational work you do here, the greater  
                // the speedup compared to a sequential foreach loop. 
                string filename = System.IO.Path.GetFileName(currentFile);
                System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(currentFile);

                bitmap.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);
                bitmap.Save(System.IO.Path.Combine(newDir, filename));
            }

            s.Stop();
            Console.WriteLine(s.Elapsed);
        }

        static void ParallelFlipper()
        {
            Stopwatch s = new Stopwatch();
            s.Start();

            // A simple source for demonstration purposes. Modify this path as necessary. 
            string[] files = System.IO.Directory.GetFiles(@"Sample Pictures", "*.jpg");
            string newDir = @"Sample Pictures\Modified";
            System.IO.Directory.CreateDirectory(newDir);

            //  Method signature: Parallel.ForEach(IEnumerable<TSource> source, Action<TSource> body)
            Parallel.ForEach(files, currentFile =>
            {
                // The more computational work you do here, the greater  
                // the speedup compared to a sequential foreach loop. 
                string filename = System.IO.Path.GetFileName(currentFile);
                System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(currentFile);

                bitmap.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);
                bitmap.Save(System.IO.Path.Combine(newDir, filename));

                // Peek behind the scenes to see how work is parallelized. 
                // But be aware: Thread contention for the Console slows down parallel loops!!!
                //Console.WriteLine("Processing {0} on thread {1}", filename,
                //                    Thread.CurrentThread.ManagedThreadId);

            } //close lambda expression
                 ); //close method invocation 

            s.Stop();
            Console.WriteLine(s.Elapsed);
        }
        public static void PrintMenu()
        {
            Console.WriteLine();
            Console.WriteLine("1: Sequential");
            Console.WriteLine("2: Parallel");
            Console.WriteLine("Q: Quit");
        }
    }
}
